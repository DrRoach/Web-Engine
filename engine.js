/**
 * Global Variables
 */
export class Engine {
    constructor(reportErrors) {
        this.REPORT_ERRORS = reportErrors || false;

        if (typeof $ == "undefined") {
            if (this.REPORT_ERRORS == true) {
                //Throw an error and end the script
                throw new Error("jQuery couldn't be found. Please include it to use the engine.");
            } else {
                //A error has to be thrown to stop the script
                throw new Error();
            }
        }
    }
}
