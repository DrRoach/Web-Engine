export class Screen {
    /**
     * Initialise the Screen class
     *
     * This method creates the Screen object and passes the Engine object that is required at
     * many different times in this class for things such as FPS and error reporting.
     *
     * @param Engine Engine object
     *
     * @since Method available since ALPHA
     */
    constructor(engine) {
        this.engine = engine;
        this.text = [];
    }

    /**
     * Create the Screen object
     *
     * This should be one of, if not, the first functions that you call after `new Engine()` 
     * in your code. It sets the canvas context and populates this class with all of the data that 
     * is required. If you try and call another Screen function before this one, you will get
     * errors.
     *
     * @param String id The ID of the canvas that you are using for your screen
     * @param int    width The width of the screen
     * @param int    height The height of the screen
     *
     * @since Method available since ALPHA
     */
    create(id, width, height) {
        this.id = id || null;
        this.ctx = null;
        this.canvas = null;
        this.background = null;
        this.x = 0;
        this.y = 0;
        this.repeat = false;
        this.width = width || null;
        this.height = height || null;
        this.tick = 1000 / this.engine.FPS;
        
        // Screen border positions
        this.TOP = 0;
        this.RIGHT = width;
        this.BOTTOM = height;
        this.LEFT = 0;

        // User helper variables
        this.LEFT = this.x;
        this.RIGHT = this.width;

        if (this.id === null) {
            this.engine.Error.throw('You must provide the canvas ID in your `Screen.create()` call');
        }

        if (this.width === null) {
            this.engine.Error.throw('You must provide a width in your `Screen.create()` call');
        }

        if (this.height === null) {
            this.engine.Error.throw('You must provide a height in your `Screen.create()` call');
        }

        this.canvas = document.getElementById(this.id);

        // If the canvas hasn't been found then throw error
        if (this.canvas === null) {
            this.engine.Error.throw("No canvas could be ound with the ID: " + this.id);
        }

        // Make sure that our canvas width and height are correct
        if (this.canvas.width != this.width || this.canvas.height != this.height) {
            this.setSize(this.canvas, this.width, this.height);
        }

        // Set our context
        this.ctx = this.canvas.getContext("2d");
    }

    /**
     * Set the size of the Screen
     *
     * Set the size of the screen that the user is playing on. This function is called from 
     * `Screen.create()` which is the first function that you should call.
     *
     * @param canvas canvas The canvas object to be resized
     * @param int    width  The width of the screen
     * @param int    height The height of the screen
     *
     * @since Method available since ALPHA
     */
    setSize(canvas, width, height) {
        canvas.setAttribute('width', this.width);
        canvas.setAttribute('height', this.height);
    }

    /**
     * Set the background of the Screen
     *
     * Draw your background onto the Screen. You can set where to start drawing the background 
     * using the `x` and `y` parameters and you can choose the width and height of the background 
     * using the `w` and `h` parameters. You can also set the background to repeat using the 
     * `repeat` parameter. If `repeat` is set to `true` then it will fill the whole screen
     * starting from the `x`,`y` position given, else it will draw once.
     *
     * @param String background The path to your background image
     * @param int    x          Where on the x axis you want your background to start
     * @param int    y          Where on the y axis you want your background to start
     * @param int    w          The width you want your background to be drawn to
     * @param int    h          The height you want your background to be drawn to
     * @param boolean           Whether or not you want your background to be repeated
     *
     * @since Method available since ALPHA
     */
    setBackground(background, x, y, w, h, repeat) {
        // If no background is defined then we have errors
        if (typeof background === "undefined" || background === null) {
            this.engine.Error.throw("No background has been set in `Screen.setBackground()`");
        }

        // Set our default values
        x = x || 0;
        y = y || 0;
        w = w || this.width;
        h = h || this.height;
        repeat = repeat || false;


        // Make sure we have access to `this` in anonymous methods
        let self = this;
        
        // If background starts with # then assume it's being set to colour
        if (background.substr(0, 1) == "#") {
            this.background = background;
            drawBackground(background, true);
            return;
        }

        // If background is currently null then we need to load the background image
        if (this.background === null) {
            // Load the background image
            let backgroundImage = new Image();
            backgroundImage.src = background;
            // Once the image has loaded we can draw it
            backgroundImage.addEventListener('load', function() {
                this.background = backgroundImage;
                drawBackground(backgroundImage);
            });
        } else {
            let backgroundImage = this.background;
            drawBackground(backgroundImage);
        }

        function drawBackground(image, colour) {
            colour = colour || false;

            // If the image is repeated draw it until the canvas is full
            if (repeat === true) {
                // Loop over all values across the x axis
                for (let xAxis = x; xAxis < self.width; xAxis += w) {
                    // Loop over all values across the y axis
                    for (let yAxis = y; yAxis < self.height; yAxis += h) {
                        if (colour) {
                            self.ctx.fillStyle = image;
                            self.ctx.fillRect(xAxis, yAxis, w, h);
                        } else {
                            self.ctx.drawImage(image, xAxis, yAxis, w, h);
                        }
                    }
                }
            } else {
                if (colour) {
                    self.ctx.fillStyle = image;
                    self.ctx.fillRect(x, y, w, h);
                } else {
                    self.ctx.drawImage(image, x, y, w, h);
                }
            }

            // Draw the text onto the screen
            for (let pos = 0; pos < self.text.length; pos++) {
                let textObject = self.text[pos];

                self.ctx.font = textObject.font;
                self.ctx.fillStyle = textObject.color;
                self.ctx.fillText(textObject.text, textObject.x, textObject.y);
            }
        }
    }

    /**
     * Clear the screen and redraw the background
     *
     * When something needs to move on the screen, this function will clear the whole screen and then
     * redraw the background onto the screen in a way that doesn't make the whole game lag.
     *
     * @since Method available since ALPHA
     */
    clear() {
        this.ctx.clearRect(0, 0, this.width, this.height);
        
        // Check to see if background is set
        if (this.background !== null) {
            //Whenever the screen is cleared, automatically redraw the background
            this.setBackground(this.background, this.x, this.y, this.backgroundwidth,
                this.backgroundheight, this.repeat);
        }
    }

    /**
     * Run a function after a set amount of time
     *
     * Can be used to give a countdown before a start of a game for example.
     *
     * @param float    milliseconds The number of milliseconds for the timer to wait for 
     * @param function callback     The callback function to be ran when the timer gets to the 
     *                                  desired time
     *
     * @since Method available since ALPHA
     */
    timer(milliseconds, callback) {
        setTimeout(callback, milliseconds);
    }

    addText(textObject) {
        this.text.push(textObject);
    }

    removeText(textObject) {
        for (let pos = 0; pos < this.text.length; pos++) {
            let savedTextObject = this.text[pos];

            if (savedTextObject.id == textObject.id) {
                this.text.splice(pos, 1);
                return;
            }
        }
    }
}
