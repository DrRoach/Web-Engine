export class Text {
    constructor(text, x, y) {
        this.font = "20px Georgia";
        this.text = text;
        this.x = x;
        this.y = y;
        this.color = "black";
        this.id = this.generateId();
    }

    setFont(font, size) {
        this.font = size + "px " + font;
    }

    setText(text) {
        this.text = text;
    }

    setColor(color) {
        this.color = color;
    }

    generateId() {
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        let id = "";
        // Generate 32 character string
        for (let pos = 0; pos < 32; pos++) {
            id += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return id;
    }
}
