import { EngineError } from './errors/EngineError.js';
import { Screen } from './screen/Screen.js';


export class Engine {
    /**
     * Create new Engine object
     *
     * This method creates a new Engine object and sets a couple of default values.
     *
     * @param boolean reportErrors Whether or not detailed errors should be displayed in the
     *  console
     *
     * @since Method available since ALPHA
     */
    constructor(reportErrors) {
        // Set default values
        this.Error = new EngineError(reportErrors);
        this.FPS = 60;

        this.Screen = new Screen(this);
    }
}
