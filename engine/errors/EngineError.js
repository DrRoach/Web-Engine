export class EngineError {
    constructor(reportErrors) {
        this._reportErrors = reportErrors || false;
    }

    throw(message) {
        if (this._reportErrors) {
            throw new Error(message);
        } else {
            throw new Error();
        }
    }
}
