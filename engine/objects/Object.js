import { Sprite } from '../draw/Sprite.js';

export class Object {
    constructor(engine) {
        this.engine = engine;

        this.sprite = null;
        this.direction = null;
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
        this.collisionDetection = [];
        this.keys = [];
        this.movements = [];

        this.TOP = this.y;
        this.RIGHT = this.x + this.width;
        this.BOTTOM = this.y + this.height;
        this.LEFT = this.x;
    }

    /**
     * Set the objects' size
     *
     * It allows the engine to know how big of a sprite should be drawn when required and it
     * also allows the engine to do logical things with the object such as collision 
     *  detection.
     *
     * @param int width  The width of the object
     * @param int height The height of the object
     *
     * @since Method available since ALPHA
     */
    setSize(width, height) {
        if (width % 1 != 0 || height % 1 != 0) {
            this.engine.Error.throw("Both width and height must be int");
        }

        this.width = width;
        this.height = height;

        // Update object constants
        this.calculateBoundaryConstants();
    }

    calculateBoundaryConstants() {
        this.TOP = this.y;
        this.RIGHT = this.x + this.width;
        this.BOTTOM = this.y + this.height;
        this.LEFT = this.x;
    }

    /**
     * Set the objects' sprite image
     *
     * Use this function to add an image to the sprite so that people can see it in your game.
     * setSprite also calls setSize so that you don't need to make multiple function calls.
     *
     * @param String image  The name of the image
     * @param int    width  The width of the sprite
     * @param int    height The height of the sprite
     *
     * @since Method available since ALPHA
     */
    setSprite(image, width, height) {
        let img = new Image();
        img.src = image;
        img.addEventListener('load', this.cacheSprite.bind(null, this, img));

        this.setSize(width, height);

        this.sprite = image;
    }

    /**
     * Function to be used as callback to cache image
     *
     * @param object
     * @param image
     */
    cacheSprite(object, image) {
        object.sprite = image;
    }

    /**
     * Draw the object onto the screen
     *
     * Use this to draw the objects' sprite onto the screen. This function can only be used after 
     * `setSprite()` has been called.
     *
     * @param int x The x position of where the object should be drawn
     * @param int y The y position of where the object should be drawn
     *
     * @since Method available since ALPHA
     */
    draw(x, y) {
        // For loop to detect object collisions
        for (let i = 0; i < this.collisionDetection.length; i++) {
            let cur = this.collisionDetection[i];
            let obj = cur.obj;
            if ((this.x >= obj.x && this.x <= (obj.x + obj.width)) || (this.x <= obj.x && (this.x + this.width) >= obj.x) || (obj.x == null)) {
                if ((this.y >= obj.y && this.y <= (obj.y + obj.height)) || (this.y <= obj.y && (this.y + this.height) >= obj.y) || (obj.y == null)) {
                    cur.callback();
                }

            }
        }

        // For loop to handle key presses
        for (let i = 0; i < this.keys.length; i++) {
            if (this.keys[i].pressed == true) {
                this.keys[i].callback();
            }
        }

        for (let i = 0; i < this.movements.length; i++) {
            let obj = this.movements[i];
            this.move(obj.direction, obj.speed);
        }

        x = x || this.x;
        y = y || this.y;

        var sprite = new Sprite(this.engine.Screen);
        sprite.drawSprite(this.sprite, x, y, this.width, this.height);
    }

    /**
     * Set the position of the object
     *
     * Set the position of the object on the screen. You can also use the Screen position
     * constants such as Screen.RIGHT to set the position correctly.
     *
     * @param int/String x The `x` position of the object
     * @param int/String y The `y` position of the object
     *
     * @since Method available since ALPHA
     */
    setPosition(x, y) {
        if (typeof x == "string") {
            switch (x.toLowerCase()) {
                case 'left':
                    x = 0;
                    break;
                case 'right':
                    x = this.engine.Screen.WIDTH - this.engine.Screen.WIDTH;
                    break;
            }
        }
        if (typeof y == "string") {
            switch(y.toLowerCase()) {
                case 'top':
                    y = 0;
                    break;
                case 'bottom':
                    y = this.engine.Screen.HEIGHT - this.engine.HEIGHT;
                    break;
            }
        }
        this.x = x;
        this.y = y;

        this.calculateBoundaryConstants();
    }

    /**
     * Bind callbacks to specific keys
     *
     * Bind user defined callback to keys that are pressed. These callbacks are looped over once
     *  per frame.
     *
     * @param int      key      The keycode to bind the callback to
     * @param function callback The method that will be called when the given key is pressed
     *
     * @since Method available since ALPHA 
     */
    keyPress(key, callback) {
        this.keys.push({
            "key": key,
            "pressed": false,
            "callback": callback
        });
    }

    move(x, y) {
        this.x = x;
        this.y = y;
    }


	/**
	 * Function that handles key presses
	 *
	 * This is a function that should only be called from inside of the Object class.
	 * It is a callback function for both the keydown and keyup event listeners
	 *
	 * TODO: Add this to README
	 *
	 * @param Object obj The Object object
	 * @param String type The type of key event
	 * @param KeyPressEvent event The keypress event that occurred
	 */
	keyListen(type, self, event) {
		for (var i = 0; i < self.keys.length; i++) {
			if (self.keys[i].key == event.keyCode) {
				if (type == 'down') {
					self.keys[i].pressed = true;
				} else {
					self.keys[i].pressed = false;
				}
			}
		}
	}

	listen() {
		document.addEventListener('keydown', this.keyListen.bind(null, 'down', this));
		document.addEventListener('keyup', this.keyListen.bind(null, 'up', this));
	}	

    detectCollision(obj, callback) {
        if (typeof obj == "string") {
            let side = obj;
            obj = {};

            switch (side.toLowerCase()) {
				case 'top':
                    obj.X = null;
                    obj.Y = 0;
                    obj.HEIGHT = 0;
                    break;
                case 'right':
                    obj.X = Screen.WIDTH;
                    obj.Y = null;
                    obj.WIDTH = 0;
                    break;
                case 'bottom':
                    obj.Y = Screen.HEIGHT;
                    obj.X = null;
                    obj.HEIGHT = 0;
                    break;
                case 'left':
                    obj.X = 0;
                    obj.Y = null;
                    obj.WIDTH = 0;
                    break;
            }
        }

		this.collisionDetection.push({obj: obj, callback, callback});
    }
}
